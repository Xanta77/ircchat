package Uno.Main;

import Uno.Socket.TCPSocket;
import Uno.Transceiver.ReaderPrinter;
import Uno.Transceiver.Transceiver;
import Uno.User.User;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class NetCat {
	public static List<User> user = new LinkedList<User>();
	public static boolean servermode = false;

	public static void main(String[] args) throws IOException {
		Transceiver trans;
		if (args.length != 2) {
			System.err.println("Falsche start argumente -l für listen oder zieladdresse\n2 argument ist der Port.\n Du hattest:"+args.length);
		} else {

			int port = Integer.parseInt(args[1]);
			String host = args[0];
			if (args[0].equals("-l")) {
				// Server
				System.out.println("Server");
				servermode = true;
				Socket socket;
				ReaderPrinter readerPrinter = new ReaderPrinter(System.in,System.out,null,null);
				ServerSocket serverSocket = new ServerSocket(port);
				while(true) {
					socket = serverSocket.accept();
					trans = new Transceiver(new TCPSocket(socket), readerPrinter);//localhost port w�hlbar
					readerPrinter.setTarget(trans);
					trans.start();
					readerPrinter.listen();
				}
			} else {
				// CLient
				System.out.println("Client");

				ReaderPrinter readerPrinter = new ReaderPrinter(System.in,System.out,null,null);
				Socket socket = new Socket(host, port);
				trans = new Transceiver(new TCPSocket(socket),readerPrinter);//ziel ip und ziel port
				readerPrinter.setTarget(trans);
				trans.start();
				readerPrinter.listen();
				trans.tell("NICK DANIEL",null);
				trans.tell("USER DANIEL * * :2cat",null);
			}
		}
	}
}
