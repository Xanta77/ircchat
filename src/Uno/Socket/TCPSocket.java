package Uno.Socket;


import java.io.*;
import java.net.Socket;

public class TCPSocket {


	public Socket socket;


	public TCPSocket(Socket socket) {
		this.socket = socket;
	}


	public void send(String s) throws IOException {
		DataOutputStream out = new DataOutputStream(socket.getOutputStream());
		out.writeUTF(s);
		out.flush();
	}

	public String receive() throws IOException {
		DataInputStream in = new DataInputStream(socket.getInputStream());
		return in.readUTF();
	}

	public void close(){
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
