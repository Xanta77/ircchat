package Uno.User;

import Uno.Transceiver.Actor;

public class User {
	String nickname;
	String username;
	String realname;
	boolean hasReceivedWelcome = false;
	Actor<String> Transceiver;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Actor<String> getTransceiver() {
		return Transceiver;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public void setTransceiver(Actor<String> transceiver) {
		Transceiver = transceiver;
	}

	public boolean longgedIn(){
		if(nickname != null && username != null){
			return true;
		}
		return false;
	}

	public boolean isHasReceivedWelcome() {
		return hasReceivedWelcome;
	}

	public void setHasReceivedWelcome(boolean hasReceivedWelcome) {
		this.hasReceivedWelcome = hasReceivedWelcome;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof User){
			User newUser = (User)obj;
			return (this.nickname).equals(newUser.nickname);
		}
		if(obj instanceof String){
			return this.nickname.equals(obj);
		}
		return false;
	}

	public void reset() {
		nickname = null;
		username = null;
		realname = null;
		hasReceivedWelcome = false;
	}
}
