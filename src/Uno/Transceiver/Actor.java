package Uno.Transceiver;

public interface Actor<A> {
	void tell(String msg, Actor<A> sender);
	void shutdown();
}
