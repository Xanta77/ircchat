package Uno.Transceiver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Reader implements Runnable {

	private Actor<String> transceiver;
	private Actor<String> sender;
	private InputStream in;

	public Reader(InputStream in, Actor<String> transceiver, Actor<String> sender) {
		this.transceiver = transceiver;
		this.sender = sender;
		System.out.println("Uno.Transceiver.Reader Initialisiert");
		this.in = in;
	}
	public void setTarget(Actor transceiver) {
		this.transceiver = transceiver;
	}

	@Override
	public void run() {
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		// Hier kommt der Interpreter code hin
		String s;
		try {
			while ((s = br.readLine()) != null) {
				transceiver.tell(s, null);
			}
			transceiver.tell("\u0004",null);
			transceiver.shutdown();
		} catch (IOException e) {
			System.err.println(e);
		}
	}

}
