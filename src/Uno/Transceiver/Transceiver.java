package Uno.Transceiver;

import Uno.Socket.TCPSocket;

public class Transceiver extends Thread implements Actor {
	private Receiver receiver;
	private final Actor transmitter;
	private Actor actor;

	public Transceiver(TCPSocket socket, Actor actor){
		// startet Uno.Transceiver.Transmitter
		this.transmitter = new Transmitter(socket);

		receiver = new Receiver(socket, actor);

	}


	public void setActor(Actor actor) {
		this.actor = actor;
	}
	@Override
	public void tell(String msg, Actor sender) {
		transmitter.tell(msg,sender);
	}

	@Override
	public void shutdown() {

	}

	@Override
	public void run() {
		receiver.setTransceiver(this);
		new Thread(receiver).start();
	}
}
