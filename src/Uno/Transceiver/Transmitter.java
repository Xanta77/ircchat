package Uno.Transceiver;

import Uno.Socket.TCPSocket;

import java.io.IOException;

public class Transmitter implements Actor {
	private final TCPSocket socket;



	public Transmitter(TCPSocket socket) {
		// Debug ausgabe
		System.out.println("Uno.Transceiver.Transmitter Initialisiert");
		this.socket = socket;
	}

	@Override
	public void tell(String msg, Actor empfaenger) {
		try {
			socket.send(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void shutdown() {
		System.exit(0);
	}
}
