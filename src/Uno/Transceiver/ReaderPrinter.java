package Uno.Transceiver;

import java.io.InputStream;
import java.io.PrintStream;


public class ReaderPrinter implements Actor {
	private Actor actor;
	private final Reader reader;

	public ReaderPrinter(InputStream in, PrintStream out,Actor transceiver, Actor sender) {
		// Debug Ausgabe
		this.actor = new Printer(out);
		System.out.println("actor Initialisiert");
		reader = new Reader(in, transceiver,null);

	}

	public void listen() {
		new Thread(reader).start();
	}
	public void setTarget(Actor actor) {
		this.reader.setTarget(actor);
	}
	@Override
	public void tell(String msg, Actor sender) {
		actor.tell(msg, sender);
	}

	@Override
	public void shutdown() {
		System.exit(0);
	}
}
