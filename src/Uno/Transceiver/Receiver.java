package Uno.Transceiver;

import Uno.Main.NetCat;
import Uno.Socket.TCPSocket;
import Uno.Transceiver.Util.Parser;

import java.io.IOException;

import Uno.User.User;

import static Uno.Main.NetCat.servermode;

public class Receiver implements Runnable {
	private final TCPSocket socket;
	private final Actor printer;
	private Actor transceiver;

	public Receiver(TCPSocket socket, Actor printer) {
		// Debug Ausgabe
		System.out.println("Uno.Transceiver.Receiver Initialisiert");
		this.socket = socket;
		this.printer = printer;
	}


	@Override
	public void run() {
		User user = new User();
		String adresse = "";
		Parser parser = new Parser();
		user.setTransceiver(this.transceiver);
		while (true) {
			String msg = null;
			try {
				msg = socket.receive();
			} catch (IOException e) {

			}
			if (msg == null) {
				if (NetCat.user.contains(user)) {
					NetCat.user.remove(user);
					printer.tell(user.getNickname() + " has disconnected", null);
				}
			} else {


				if (servermode) {
					parser.setMsg(msg);
					if (!user.longgedIn()) {
						if (parser.foundNick()) {
							user.setNickname(parser.getNickname());
						}
						if (parser.foundUser()) {
							user.setUsername(parser.getUsername());
							user.setRealname(parser.getRealname());
							if (!NetCat.user.contains(user)) {
								user.getTransceiver().tell("Login successfull", null);
								NetCat.user.add(user);
							} else {
								user.getTransceiver().tell("Nick already in use, Uno.User will be reseted", null);

								user.reset();
							}
						}
					} else {
						if (parser.hasAdresse()) {
							adresse = parser.getAdresseNick();
						}
						for (User u : NetCat.user) {
							if (u.equals(adresse)) {
								msg = user.getNickname()+":"+msg.substring(adresse.length());
								u.getTransceiver().tell(msg, null);
							}
						}

					}
					printer.tell(user.getNickname() + ": " + msg, null);
				} else {
					printer.tell(msg, null);
				}

			}

		}
	}

	public void setTransceiver(Actor transceiver) {
		this.transceiver = transceiver;
	}
}
