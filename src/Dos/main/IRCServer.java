package Dos.main;

import Dos.socket.TCPSocket;
import Dos.transceiver.ReaderPrinter;
import Dos.transceiver.Transceiver;
import Dos.transceiver.util.User;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class IRCServer {
    public static List<User> user = Collections.synchronizedList(new LinkedList<User>());
    public static boolean servermode = false;
    public static String netWorkName = "Asoziales Netzwerk";
    public static String host ="localhost";
    public static String date;

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.err.println("Falsche start argumente -l für listen oder zieladdresse\n2 argument ist der Port.");
        } else {
            date = Calendar.getInstance().getTime().toString();
            Transceiver trans;
            String host = args[0];
            int port = Integer.parseInt(args[1]);
            if (args[0].equals("-l")) {
                // Server
            	System.out.println("Server");
            	servermode = true; // TODO error when turned on if not nop err occours
                Socket socket;
                ReaderPrinter readerPrinter = new ReaderPrinter(System.in,System.out,null,null);
                ServerSocket serverSocket = new ServerSocket(port);
                while(true) {
                    socket = serverSocket.accept();
                    trans = new Transceiver(new TCPSocket(socket), readerPrinter);//localhost port w�hlbar
                    readerPrinter.setTarget(trans);
                    trans.init();
                    readerPrinter.listen();
                }
            } else {
                // CLient
            	System.out.println("Client");
                ReaderPrinter readerPrinter = new ReaderPrinter(System.in,System.out,null,null);
                Socket socket = new Socket(host, port);
                trans = new Transceiver(new TCPSocket(socket),readerPrinter);//ziel ip und ziel port
                readerPrinter.setTarget(trans);
                trans.init();
                readerPrinter.listen();
                trans.tell("NICK Daniel",null);
                trans.tell("USER BOB * * :Kuchen",null);
                trans.tell("PRIVMSG Daniel :HI",null);
            }
        }
    }
}
