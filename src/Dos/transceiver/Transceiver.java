package Dos.transceiver;

import Dos.socket.TCPSocket;

public class Transceiver  implements Actor {
    private Receiver receiver;
    private Transmitter transmitter;
    private Actor actor;

    public Transceiver(TCPSocket socket, Actor<String> actor){
        this.transmitter = new Transmitter(socket);
        receiver = new Receiver(socket, actor);
    }
    public void init(){
        receiver.setTransceiver(this);
        new Thread(receiver).start();
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    @Override
    public void tell(String msg, Actor sender) {
        transmitter.tell(msg,sender);
    }

    @Override
    public void shutdown() {
        receiver.close();
    }



}
