package Dos.transceiver.util;

import Dos.main.IRCServer;
import Dos.transceiver.Actor;

public class User implements Comparable{
    public User(Actor<String> transceiver) {
        this.transceiver = transceiver;
    }

    private String realname;
    private String username;
    private String nickname;
    private boolean receivedWelcome = false;
    private Actor<String> transceiver;

    public boolean hasReceivedWelcome() {
        return receivedWelcome;
    }

    public String getRealname() {
        return realname;
    }

    public String getUsername() {
        return username;
    }

    public Actor<String> getTransceiver() {
        return transceiver;
    }

    public String getNickname() {
        return nickname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setReceivedWelcome(boolean receivedWelcome) {
        this.receivedWelcome = receivedWelcome;
    }


    /**
     * return true, if nickname was set succesfully
     * and was added to IRCServer.user
     * internaly case doesnt matter
     *
     *
     * @param newNick
     * @return
     */
    public boolean setNickname(String newNick) {
        boolean newNickMatch = IRCServer.user.stream().anyMatch(user -> user.getNickname().toLowerCase().equals(newNick.toLowerCase()));
        if (nickname != null) {
            if (!newNickMatch) {
                this.nickname = newNick;
                return true;
            }
        } else if (!newNickMatch) {
            nickname = newNick;
            IRCServer.user.add(this);
            return true;
        }
        return false;
    }


    public void setTransceiver(Actor<String> trans) {
        transceiver = trans;
    }

    public boolean loggedIn() {
        return nickname != null && username != null;
    }


    @Override
    public String toString() {
        return String.format("Nick[%s],User[%s],real[%s]", nickname, username, realname);
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    public void quit() {
        transceiver.shutdown();
        transceiver = null;
        IRCServer.user.remove(this);
    }
}
