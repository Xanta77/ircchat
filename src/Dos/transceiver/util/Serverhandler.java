package Dos.transceiver.util;

import Dos.main.IRCServer;
import Dos.transceiver.Actor;

public class Serverhandler {
    private User user;
    private Actor<String> printer;
    private final String DELIM = "\r\n";

    public Serverhandler(Actor<String> printer, Actor transceiver) {
        this.user = new User(transceiver);
        this.printer = printer;
    }


    public void handle(String msg) {
        IRCMessage ircMessage = null;
        try {
            ircMessage = IRCParser.parse(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }


        printer.tell(String.format("<%s> :%s", user.getNickname(), msg), null);
        if (ircMessage != null) {
            if (!user.loggedIn()) {
                switch (ircMessage.getCommand()) {
                    case "NICK":
                        doNickStuff(ircMessage);
                        break;
                    case "USER":
                        doUserStuff(ircMessage);
                        break;
                    default:
                        send_ERR_NOTREGISTERED();
                        break;
                }
            } else {
                // Send welcome
                if (user.loggedIn() && !user.hasReceivedWelcome()) {
                    send_RPL_WELCOME();
                    send_RPL_YOURHOST();
                    send_RPL_CREATED();
                    send_RPL_MYINFO();
                }

                switch (ircMessage.getCommand()) {
                    case "NICK":
                        doNickStuff(ircMessage);
                        break;
                    case "USER":
                        doUserStuff(ircMessage);
                        break;
                    case "QUIT":
                        rpl_quit(String.join(" ", ircMessage.getMiddle()));
                        break;
                    case "PRIVMSG":
                        sendMsgTo(ircMessage.getParameters()[0], String.join(" ", ircMessage.getParameters()).substring(ircMessage.getParameters()[0].length() + 1));
                        break;
                    case"PING":
                        send("PONG");
                        break;
                    case"PONG":
                        printer.tell(String.format("Received Pong from %s",user.getNickname()),null);
                        break;
                    default:
                        send_ERR_UNKNOWNCOMMAND(ircMessage.getCommand());
                        break;
                }
            }
        }
    }

    private void send_RPL_MYINFO() {
        send(String.format("<%s> <%s> <%s> <%s> <%s> [<%s>]",user.getNickname(),IRCServer.host,"0.1a","none","none","none"));
    }

    private void send_ERR_UNKNOWNCOMMAND(String command) {
        send(String.format("<%s> <%s> :Unknown command",user.getNickname(),command));
    }

    private void doUserStuff(IRCMessage ircMessage) {
        if (user.loggedIn()) {
            send_ERR_ALREADYREGISTRED();
        } else {
            user.setUsername(ircMessage.getParameters()[0]);
            user.setRealname(ircMessage.getParameters()[3]);
        }
    }

    private void doNickStuff(IRCMessage ircMessage) {
        if (!ircMessage.getMiddle().equals("")) {
            if (user.setNickname(ircMessage.getParameters()[0])) {
                if (user.loggedIn()) {
                    send_NICKNAME_CHANGED();
                }
            } else send_ERR_NICKNAME_TAKEN(ircMessage.getParameters()[0]);
        } else send_ERR_NONICKNAMEGIVEN();
    }

    private void send_RPL_CREATED() {
        send(String.format("This server was created <%s>", IRCServer.date));
    }

    private void send_ERR_NONICKNAMEGIVEN() {
        send(":No nickname given");
    }

    private void send_NICKNAME_CHANGED() {
        send(String.format("<%s> is now your Nickname!", user.getNickname()));

    }

    private void send(String str) {
        user.getTransceiver().tell(str + DELIM, null);
    }

    private void sendMsgTo(String nick, String msg) {
        if (user.loggedIn()) {
            IRCServer.user.forEach(u -> {
                if (u.getNickname().equals(nick) && u.loggedIn()) {
                    u.getTransceiver().tell(String.format("<%s> :%s", user.getNickname(), msg + DELIM), null);
                }
            });
        }
    }

    private void rpl_quit(String msg) {
        if (msg.equals("")) {
            msg = String.format("%s Quit", user.getNickname());
        }
        send(String.format("Closing Link: %s (%s)", IRCServer.host, msg));
        user.quit();
    }

    private void send_RPL_WELCOME() {
        send(String.format("Welcome to the %s Network, <%s>!<%s>@<%s>", IRCServer.netWorkName, user.getNickname(), user.getUsername(), IRCServer.host));
        user.setReceivedWelcome(true);
    }

    private void send_ERR_ALREADYREGISTRED() {
        send(":You may not reregister");
    }

    private void send_ERR_NICKNAME_TAKEN(String nick) {
        send(String.format("<%s> :Nickname is already in use", nick));
    }

    private void send_RPL_YOURHOST() {
        send(String.format("Your host is <%s>, running version <%s>", IRCServer.host, "0.1a"));
    }

    private void send_ERR_NOTREGISTERED() {
        send(":Your have not registered");
    }
}
