package Dos.transceiver;


import Dos.socket.TCPSocket;
import Dos.transceiver.util.Serverhandler;
import Dos.transceiver.util.User;

import java.io.IOException;

import static Dos.main.IRCServer.servermode;


public class Receiver extends Thread {
    private final TCPSocket socket;
    private final Actor<String> printer;
    private Actor<String> transceiver = null;
    private boolean running = true;

    public Receiver(TCPSocket socket, Actor<String> printer) {
        this.socket = socket;
        this.printer = printer;
    }

    @Override
    public void run() {
        Serverhandler serverhandler = new Serverhandler(printer,this.transceiver);
        while (running) {
            String msg = null;

            try {
                msg = socket.receive();
            } catch (IOException ignored) {
            }
            if(msg == null){
                running = false;
            }
            else if(msg.equals("/u0004")){
               running = false;
            }

            if(running) {
                if (servermode) serverhandler.handle(msg);
                else printer.tell(msg, null);
            }
        }
    }

    public void setTransceiver(Actor<String> transceiver) {
        this.transceiver = transceiver;
    }
    public void close(){
        running = false;
    }
}
