package Tres.Transceivers;

import Tres.Socket.TCPSocket;

import java.io.IOException;

public class Transceiver implements Actor {
    private Receiver receiver;
    private Transmitter transmitter;
    private TCPSocket socket;
    private Actor<String> readerPrinter;
    private Actor<String> serverhandler;

    public Transceiver(TCPSocket socket) {
        this.socket = socket;
        this.serverhandler = new Serverhandler();
        this.transmitter = new Transmitter(socket);



    }

    public void start(Actor<String> readerPrinter) {
        this.readerPrinter = readerPrinter;
        this.receiver = new Receiver(socket,this);
        new Thread(receiver).start();
    }

    @Override
    public void tell(String msg, Actor sender, Actor receiver) throws IOException {
        serverhandler.tell(msg, this.transmitter, this.readerPrinter);
    }


    @Override
    public void shutdown() {
        receiver.close();
    }

}
