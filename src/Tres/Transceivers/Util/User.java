package Tres.Transceivers.Util;

import Tres.main.IRCServer;
import Tres.Transceivers.Actor;

public class User {

    private String realname;
    private String username;
    private String nickname;
    private boolean receivedWelcome = false;
    private Actor<String> transceiver;
    private String mode;
    private boolean away = false;

    public String getAwayMsg() {
        return awayMsg;
    }

    public void setAwayMsg(String awayMsg) {
        this.awayMsg = awayMsg;
    }

    private String awayMsg = "";

    public String getRealname() {
        return realname;
    }

    public void setAway(Boolean away) {
        this.away = away;
    }

    public boolean getAway() {
        return away;
    }
    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public boolean hasReceivedWelcome() {
        return receivedWelcome;
    }

    public void setReceivedWelcome(boolean receivedWelcome) {
        this.receivedWelcome = receivedWelcome;
    }

    public Actor<String> getTransceiver() {
        return transceiver;
    }

    public void setTransceiver(Actor<String> transceiver) {
        this.transceiver = transceiver;
    }

    /**
     * and was added to IRCServer.user
     * internaly case doesnt matter
     *
     * @param newNick
     * @return
     */
    public String setNickname(String newNick) {
        boolean newNickMatch = IRCServer.user.stream().anyMatch(user -> user.getNickname().toLowerCase().equals(newNick.toLowerCase()));
        if (newNick == null) return "ERR_NONICKNAMEGIVEN";
        if (newNickMatch) return "ERR_NICKNAMEINUSE";
        // Falls der User schon einen Nick hatte
        if (nickname != null) {
            this.nickname = newNick;
            return "";
        }

        // Falls der User keinen Nick hatte
        nickname = newNick;
        IRCServer.user.add(this);
        return "";
    }

    public boolean loggedIn() {
        return nickname != null && username != null;
    }

    public String getMode() {
        return mode;
    }

    @Override
    public String toString() {
        return String.format("Nick[%s],User[%s],real[%s]", nickname, username, realname);
    }

    public void setMode(String s) {
        if(s.equals("a") || s.equals("o") || s.equals("")){
            this.mode = "a";
        }
    }
}
