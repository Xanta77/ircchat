package Tres.Transceivers;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Printer implements Actor {
    private PrintStream out;

    public Printer(PrintStream out){
        this.out = out;
    }
    @Override
    public void tell(String msg, Actor sender, Actor receiver) {
        this.out.println(new SimpleDateFormat("HH:mm").format(new Date())+" -- "+msg);
        //this.out.print(msg);
    }

    @Override
    public void shutdown() {
        System.exit(0);
    }
}