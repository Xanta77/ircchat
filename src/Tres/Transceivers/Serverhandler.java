package Tres.Transceivers;


import Tres.main.IRCServer;
import Tres.Transceivers.Util.User;

import java.io.*;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Serverhandler implements Actor<String> {
    User user = new User();
    private final String DELIM = "\r\n";
    String ver = "1.0";
    @Override
    public void tell(String msg, Actor<String> sender, Actor<String> receiver) throws IOException {
        this.handle(msg, sender, receiver);
    }


    @Override
    public void shutdown() {

    }


    //-------------------------------------------------//
    // Oberhalb nichts verändern                       //
    //-------------------------------------------------//

    // BSP für Ausgabe
    // http://www.networksorcery.com/enp/protocol/irc.htm
    private Map<String, String> createMap() {
        Map<String, String> myMap = new HashMap();
        myMap.put("RPL_WELCOME", "Welcome to the Internet Relay Network %s!%s@%s");
        myMap.put("RPL_YOURHOST", "Your host is %s, running version <version>");
        myMap.put("RPL_CREATED", "This server was created <date>");
        myMap.put("RPL_MYINFO", "<servername> <version> <available user modes> <available channel modes>");
        myMap.put("RPL_TOPIC", "<channel> :<topic>");
        myMap.put("RPL_NAMREPLY", "( \"=\" / \"*\" / \"@\" ) <channel> :[ \"@\" / \"+\" ] <nick> *( \" \" [ \"@\" / \"+\" ] <nick> )");
        myMap.put("ERR_NONICKNAMEGIVEN", ":No nickname given");
        myMap.put("ERR_NICKNAMEINUSE", "<client> :Nickname is already in use");
        myMap.put("RPL_ENDOFNAMES", "<channel> :End of NAMES list,");
        myMap.put("RPL_NOTOPIC", "<channel> :No topic is set");
        myMap.put("RPL_WHOISOPERATOR", "<nick> :is an IRC operator");
        myMap.put("RPL_WHOISCHANNELS", "<nick> :*( ( \"@\" / \"+\" ) <channel> \" \" )");
        myMap.put("RPL_AWAY", "<nick> :<away message");
        myMap.put("RPL_YOUREOPER", ":You are now an IRC operator");
        myMap.put("RPL_WHOISUSER", "<nick> <user> <host> * :<real name>");
        myMap.put("RPL_WHOISSERVER", "<nick> <server> :<server info>");
        myMap.put("RPL_ENDOFWHOIS", "<nick> :End of WHOIS list");
        myMap.put("ERR_NORECIPIENT", ":No recipient given (<command>)");
        myMap.put("ERR_NOTEXTTOSEND", ":No text to send");
        myMap.put("ERR_NOSUCHNICK", "<nickname> :No such nick/channel");
        myMap.put("ERR_ALREADYREGISTRED", ":Unauthorized command (already registered)");
        myMap.put("ERR_NEEDMOREPARAMS", "<command> :Not enough parameters");
        myMap.put("ERR_CANNOTSENDTOCHAN", "<channel name> :Cannot send to channel");
        myMap.put("ERR_NOTONCHANNEL", "<channel> :You're not on that channel");
        myMap.put("ERR_NOSUCHCHANNEL", "<nickname> :No such channel");
        myMap.put("ERR_NOMOTD", ":MOTD File is missing");
        return myMap;
    }

    private String AUSGABE_NACH_ANGABE = "<%s>,<%s>,<%s> Die Standard antwort lautet so";


    private String[] QUIT_MESSAGE = {
            // 0 - Quitmessage, oder Fehler sonsiter Art
            "Closing Link: HOSTNAME (%s)"
    };


    /**
     * // Das ist der Part wo die Logik hinkommt für die Nachrichten
     *
     * @param msg     String erhalten vom Receiver, wurde durch tell durchgereicht
     * @param printer //printer.tell("",null,null) für Konsolenausgabe
     * @param sender  //sender.tell("a",null,null) für TCP-Packet senden, Inhalt "a".
     */


    public void handle(String msg, Actor<String> sender, Actor<String> printer) throws IOException {
        Map<String, String> map = createMap();

        String[] singleRequestArr;
        String[] cmdAndParam;

        //während scope befüllen am ende als ein Packet rauschicken
        StringBuilder out_packet = new StringBuilder();

        singleRequestArr = msg.split(" :");
        cmdAndParam = singleRequestArr[0].split(" ");


        switch (cmdAndParam[0]) {
            case "NICK":
                String replyOrError = user.setNickname(cmdAndParam[1]);
                if (!replyOrError.equals("")) {
                    out_packet.append("<").append(user.getNickname()).append(">").append(map.get(replyOrError)).append(DELIM);///r/n == enter
                }
                break;
            case "USER":
                if (cmdAndParam.length == 4) {
                    user.setUsername(cmdAndParam[1]);
                    user.setMode(cmdAndParam[2]);
                    user.setRealname(singleRequestArr[1]);
                }
                out_packet.append("User").append(map.get("ERR_NEEDMOREPARAMS")).append(DELIM);
                break;
            case "PING":
                out_packet.append("PONG").append(DELIM);
            case "QUIT":
                IRCServer.user = IRCServer.user.stream().filter(x -> !x.getNickname().equals(user.getNickname())).collect(Collectors.toList());
                user = new User();
				out_packet.append(String.format("Closing Link: %s (%s)", IRCServer.HOST, singleRequestArr[1] != null ? singleRequestArr[1] : "Client Quit")).append(DELIM);
                break;
            case "MOTD":
                String msgotd =  motd();
                if (msgotd != null) {
                            out_packet.append(msgotd).append(DELIM);
                    } else out_packet.append(map.get("ERR_NOMOTD")).append(DELIM);
                break;
            case "LUSERS":
                for(int i=0; i <IRCServer.user.size();i++){
                    out_packet.append(IRCServer.user.get(i).toString()).append(DELIM);
                }

                break;
            case "WHOIS":
				out_packet.append(IRCServer.user.stream().filter(x->x.getNickname().equals(singleRequestArr[1])).collect(Collectors.toList()).get(0).toString()).append(DELIM);
                break;

            default:
                break;

        }


        if (user.loggedIn() && !user.hasReceivedWelcome()) {
            out_packet.append(String.format(map.get("RPL_WELCOME"), user.getNickname(), user.getUsername(), user.getRealname())).append("\r\n");
            out_packet.append(String.format(map.get("RPL_YOURHOST"), IRCServer.HOST, ver)).append("\r\n");
            user.setReceivedWelcome(true);
        }
        if (!out_packet.toString().equals("")) {
            sender.tell(out_packet.toString(), null, null);
        }
        printer.tell(msg, null, null);


        // Irgendwie so null ist glaube ich noch zu handeln
        // User user = (User) IRCServer.userMap.get(nick);
        //W
        //

        //BSP1
        //sender.tell("text", null, null);
        //BSP2
        //printer.tell("Ich bin eine Ausgabe",null,null);


        // sender.tell(msg+" Ich wiederhole in Serverhandler",null,null);
        // BSP2
        //printer.tell(String.format(AUSGABE_NACH_ANGABE,"1","2","3"),null,null);
    }

    private String motd() throws IOException {
        File file = new File(getClass().getResource("motd.txt").getPath());
        String fu = Calendar.getInstance().toString();
        int day = Integer.valueOf(fu.substring(fu.indexOf("DAY_OF_YEAR=")+"DAY_OF_YEAR=".length(),fu.indexOf(",DAY_OF_WEEK")));
        System.out.println(day);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (br.ready()) {
            for (int i = 1; i <= 366; i++) {
                if (i == day) {
                   return br.readLine();
                }
                br.readLine();
            }
        } return null;
    }
}
