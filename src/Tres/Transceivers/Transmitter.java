package Tres.Transceivers;

import Tres.Socket.TCPSocket;

import java.io.IOException;

public class Transmitter implements Actor {
	private final TCPSocket socket;



	public Transmitter(TCPSocket socket) {
		// Debug ausgabe
		this.socket = socket;
	}


	@Override
	public void tell(String msg, Actor sender, Actor receiver) {
		try {
			socket.send(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void shutdown() {
		System.exit(0);
	}
}
