package Tres.Transceivers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Reader implements Runnable {
    private Actor<String> transmitter;
	private InputStream in;

	public Reader(InputStream in, Actor<String> transceiver) {
        this.transmitter = transceiver;
		this.in = in;
	}

	@Override
	public void run() {
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String line;
		try {
			while ((line = br.readLine()) != null) {
				transmitter.tell(line, null,null);
			}
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	public void listen(Actor transceiver) {
		this.transmitter = transceiver;
	}
}
