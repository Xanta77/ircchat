package Tres.Transceivers;

import java.io.IOException;

public interface Actor<A> {
	void tell(String msg, Actor<A> sender,Actor<A> receiver) throws IOException;
	void shutdown();
}
