package Tres.Transceivers;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;


public class ReaderPrinter implements Actor {
    private Actor printer;
    private final Reader reader;

    public ReaderPrinter(InputStream in, PrintStream out) {
        // Debug Ausgabe
        this.printer = new Printer(out);
        reader = new Reader(in, null);

    }

    public void listen(Actor sender) {
        reader.listen(sender);
        new Thread(reader).start();
    }


    @Override
    public void tell(String msg, Actor sender, Actor receiver) throws IOException {
        printer.tell(msg, null,null);
    }

    @Override
    public void shutdown() {
        System.exit(0);
    }
}
