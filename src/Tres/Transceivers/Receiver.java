package Tres.Transceivers;


import Tres.Socket.TCPSocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.SocketException;


public class Receiver extends Thread {
    private final TCPSocket socket;
    private boolean running = true;
    Actor<String> actor;

    public Receiver(TCPSocket socket, Actor<String> printer) {
        this.socket = socket;
        this.actor = printer;
    }

    @Override
    public void run() {

        while (running) {
            try {
                BufferedReader in = new BufferedReader(socket.receive());
                String line;
                while ((line = in.readLine()) != null) {
                    actor.tell(line, null, null);
                }

            } catch (IOException e) {
                if (e instanceof SocketException) close();
                else e.printStackTrace();
            }
        }
    }

    public void close() {
        try {
            socket.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        running = false;
    }

}
