package Tres.main;

import Tres.Socket.TCPSocket;
import Tres.Transceivers.Actor;
import Tres.Transceivers.ReaderPrinter;
import Tres.Transceivers.Transceiver;
import Tres.Transceivers.Util.User;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class IRCServer {

	// 1D Elemente
	public static final String NETWORKNAME = "Asoziales Netzwerk";
	public static final String HOST = "localhost";

	//Variables

	// 2D Elemente
	//Liste Threadsafe
	public static List<User> user = Collections.synchronizedList(new LinkedList<User>());
	//HashMap Threadsafe
	public static ConcurrentHashMap<Actor<String>, User> userMap = new ConcurrentHashMap<Actor<String>, User>();


	public static String date;

	public static void main(String[] args) throws IOException {
		if (args.length != 2) {
			System.err.println("Falsche start argumente -l für listen oder zieladdresse\n2 argument ist der Port.");
		} else {
			date = Calendar.getInstance().getTime().toString();
			ReaderPrinter readerPrinter;
			Transceiver trans = null;
			int port = Integer.parseInt(args[1]);
			String host = args[0];
			System.out.println("Server");
			Socket socket;
			ServerSocket serverSocket = new ServerSocket(port);
			while (true) {
				socket = serverSocket.accept();
				readerPrinter = new ReaderPrinter(System.in, System.out);
				trans = new Transceiver(new TCPSocket(socket));
				trans.start(readerPrinter);
			}
		}
	}
}