package Tres.Socket;


import java.io.*;
import java.net.Socket;

public class TCPSocket {


	public Socket socket;


	public TCPSocket(Socket socket) {
		this.socket = socket;
	}


	public void send(String s) throws IOException {
		DataOutputStream  out = new DataOutputStream (socket.getOutputStream());
		out.writeBytes(s);
	}

	public InputStreamReader receive() throws IOException {
		return new InputStreamReader(socket.getInputStream());
	}

}
